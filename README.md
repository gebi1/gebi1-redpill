# gebi1-redpill-load

#### 版本介绍

0.7.0.0增加了42218以上版本的构建版本

0.7.0.1增加了所需的扩展解析添加和下载

0.7.0.2在patchdtc中增加usb补丁

0.7.0.3在patchdtc上增加端口号

0.7.0.4确保在此过程的早期创建本地缓存文件夹

0.7.0.5启用交互式

0.7.0.6增加保存/恢复会话功能

0.7.0.7增加检查日期功能

增加了使用本地dtb文件的能力

0.7.0.9增加flyride satamap审查

0.7.1.0增加历史记录、版本和增强的patchdtc功能

0.7.1.1增加syntaxcheck功能

0.7.1.2增加与NTP服务器的同步时间:pool.ntp.org(相应地设置timezone和ntpserver变量)

增加了创建JUN mod加载器的选项(通过Jumkey)

0.7.1.4增加了额外custom_config_jun的使用。创建JUN mod加载器的json

0.7.1.5更新satamap功能，支持更高的单HBA 9端口计数。

0.7.1.6更新了satamap功能，以修复损坏的q35 KVM控制器，并停止扫描光盘

更新serialgen函数，包括使用realmac的选项

0.7.1.8更新satamap功能，实现SATA端口识别微调，识别SATABOOT

0.7.1.9更新补丁dtc功能，修复VMware托管系统端口识别错误的问题

0.8.0.0稳定版本。所有新功能将被转移到开发回购

0.8.0.1更新了postupdate，方便更新到update2

0.8.0.2更新satamap，支持DUMMY PORT检测

0.8.0.3更新satamap，避免第一控制器使用0导致KP

0.8.0.4修复了缺失的二进制文件

0.8.0.5修复了listextension中的jq问题

#### 介绍
redpill-load项目都在github上，而github对国内不是很友好，导致引导制作过程无法顺利进行，这里通过优化，不需要额外的东西可以顺利完成引导的编译。

#### 软件架构
居于pocopico tinycore-redpill的项目进行优化，没有对程序进行修改，只是对链接优化。


#### 安装教程

第1节课：群晖引导编译u盘镜像tinycore-redpill下载和刻录
http://www.gebi1.com/thread-301698-1-1.html

第2节课：群晖引导编译准备篇
http://www.gebi1.com/thread-302054-1-1.html

第3节课，开始编译群晖redpill-load引导
http://www.gebi1.com/thread-302055-1-1.html





#### 使用说明
为了硬盘和数据安全，编译的时候请不要把硬盘插到机器上，你也可以用虚拟机来编译。

使用很简单，只要把本项目的rploader.sh文件上传到引导u盘，覆盖里面的旧文件即可，其他过程全部一样，记得直接拷贝的话给rploader.sh文件700可执行权限。
下载地址：
https://gitee.com/gebi1/gebi1-redpill/releases/0.1

或者直接ssh下复制下面命令直接远程下载。
`rm -f rploader.sh&&wget https://gitee.com/gebi1/gebi1-redpill-load/raw/master/rploader.sh&&sudo chmod 700 rploader.sh`

编译后pocopico tinycore-redpill默认会把引导自动刻录到u盘上。
编译后的引导文件放在redpill-load目录下的，loader.img 这个可以拷贝出来二次修改，自行写入u盘。

#### 参与贡献

感谢redpill 项目和pocopico tinycore-redpill项目 和github上一些修改的redpill的开源项目的贡献


